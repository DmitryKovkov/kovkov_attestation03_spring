package ru.pcs.attestation03_spring.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Products {
    private Integer id;
    private String description;
    private Double price;
    private Integer count;
}

