package ru.pcs.attestation03_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Attestation03SpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(Attestation03SpringApplication.class, args);
    }

}
