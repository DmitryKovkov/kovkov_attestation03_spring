package ru.pcs.attestation03_spring.repositories;

import ru.pcs.attestation03_spring.models.Products;

import java.util.List;

public interface UsersRepository {
    List<Products> findAll();
    List<Products> findAllByPrice(Double price);
    void save(Products products);
}
