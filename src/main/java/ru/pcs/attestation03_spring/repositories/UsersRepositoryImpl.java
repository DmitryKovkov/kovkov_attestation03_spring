package ru.pcs.attestation03_spring.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.attestation03_spring.models.Products;

import javax.sql.DataSource;
import java.util.List;

@Component
public class UsersRepositoryImpl implements UsersRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into product (description, price, count) values (?,CAST(?::NUMERIC AS MONEY),?)";
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from product order by id";
    //language=SQL
    private static final String SQL_SELECT_BY_PRICE = "select * from product where price = ?";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public UsersRepositoryImpl (DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Products> goodsRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        Double price = row.getDouble("price");
        int count = row.getInt("count");

        return new Products(id, description, price, count);
    };

    @Override
    public List<Products> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, goodsRowMapper);
    }

    @Override
    public List<Products> findAllByPrice(Double price) {
        return jdbcTemplate.query(SQL_SELECT_BY_PRICE, goodsRowMapper, price);
    }

    @Override
    public void save(Products products) {
        jdbcTemplate.update(SQL_INSERT, products.getDescription(), products.getPrice(), products.getCount());
    }




}
