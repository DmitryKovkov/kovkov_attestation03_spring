package ru.pcs.attestation03_spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.attestation03_spring.models.Products;
import ru.pcs.attestation03_spring.repositories.UsersRepository;

@Controller
public class ProductsController {

    //DI
    @Autowired
    private UsersRepository usersRepository;

    @PostMapping("/products")
    public String addProduct(@RequestParam("description") String description,
                             @RequestParam("price") Double price,
                             @RequestParam("count") Integer count){
        System.out.println(description + " " + price + " " + count);
        Products products = Products.builder()
                .description(description)
                .price(price)
                .count(count)
                .build();

        usersRepository.save(products);

        return "redirect:/product_add.html";
    }

}
